@extends('Admin.layout')

@section("styles")
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Obsah</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Obsah</li>
                    </ol>
                </div>
            </div>
        </div>
        @if(Auth::check() && Auth::user()->isAdmin())
            <div class="content-header-right col-md-6 col-12 mb-2">
                @if(!request()->has('deleted'))
                    <div class="btn-group mb-1 pull-right">
                        <a href="{{ route('admin.CreateFolder') }}" class="addFolder btn btn-secondary btn-block-sm"><i class="ft-folder"></i> @lang('blocks.createFolder')</a>
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="?deleted=true"><i class="ft-trash-2"></i> @lang('blocks.showDeletedFolders')</a>
                        </div>
                    </div>
                @else
                    <a href="{{ route('admin.blocks') }}" class="btn btn-secondary pull-right btn-block-sm"><i class="ft-chevron-left"></i> @lang('blocks.backToFolders')</a>
                @endif
            </div>
        @endif
    </div>
    <div class="content-body">
        @if(session('created') === true)
            <div class="alert bg-success alert-dismissible mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>@lang('blocks.folderCreatedGrats')</strong> Složka úspěšně vytvořena
            </div>
        @endif

        @if(session('created') === false)
            <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>@lang('blocks.folderNotCreatedBold')</strong> @lang('blocks.folderNotCreatedRest')
            </div>
        @endif



        @if(session('success') === true)
            <div class="alert bg-success alert-dismissible mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>@lang('blocks.folderRenamedBold')</strong>
            </div>
        @endif

        @if(session('status') === true)
            <div class="alert bg-success alert-dismissible mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>@lang('blocks.folderRestored')</strong>
            </div>
        @endif

        @if(request()->has('deleted'))
            <div class="alert bg-info alert-dismissible mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>@lang('blocks.deletedFoldersShowedBold')</strong> @lang('blocks.deletedFoldersShowedRest')
            </div>
        @endif

        @if(!request()->has('deleted'))
        <div class="row" id="left-handles">
            @if(count($folders))
                @foreach($folders as $folder)
                    <div class="col-lg-3 col-md-12">
                        <div class="card blocks-folder" data-item-id="{{ $folder->id }}">
                            <div class="card-content">
                                <div class="card-header fixed-header">
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                          <li>
                                                <a href="{{ route('admin.RenderFolder', ['id' => $folder['id']]) }}"><span class="ft-file-plus" style="color: green"></span></a>
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.renameOverfolder', ['id' => $folder['id']]) }}" class="editFolder"><span class="ft-edit" style="color: orange"></span></a>
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.RemoveFolder', ['id' => $folder->id]) }}" class="removeFolder"><span class="ft-trash-2" style="color: red"></span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-body text-center">
                                    <h4 class="card-title mb-1">{{ $folder['folder'] }}</h4>
                                    <div>
                                        @if(Auth::check() && Auth::user()->isAdmin())
                                            <code>{{ $folder['system_name'] }}</code>
                                        @endif
                                    </div>
                                    <a href="{{ route('admin.RenderFolder', ['id' => $folder['id']]) }}" class="btn btn-link">Spravovat složky</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-lg-3 col-md-12">
                    <a href="{{ route('admin.CreateFolder') }}" class="addFolder card blocks-folder">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <h4 class="card-title">@lang('blocks.noFolder')</h4>
                                <button class="btn btn-primary mt-2">@lang('blocks.createFolder')</button>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
        </div>
        @endif

        @if(Auth::user()->isAdmin() and request()->has('deleted'))
        <h3 class="content-header-title mb-1">@lang('blocks.erasedFolders')</h3>
        <div class="row">
            @if(count($erasedFolders))
                @foreach($erasedFolders as $folder)
                    <div class="col-lg-3 col-md-12">
                        <div class="card blocks-folder erased-folder">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $folder['folder'] }}</h4>
                                    <a href="{{ route('admin.RestoreOverFolder', ['id' => $folder['id']]) }}" class="restoreFolder btn btn-secondary mt-1"><i class="ft-rotate-ccw"></i> @lang('blocks.restoreFolderButton')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-lg-3 col-md-12">
                    <div href="{{ route('admin.CreateFolder') }}" class="card blocks-folder">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <h4 class="card-title">@lang('blocks.noErasedFolders')</h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        @endif
    </div>
    <div class="hidden">
        <div class="js-add-folder">
            <form id="frmAddFolder" method="get" action="">
                <div class="col-12">
                    @include('Admin.partials.multi-language-select-locale', ['setRowCol' => false])
                    <div class="form-group">
                        <label for="txtFolderName">Name</label>
                        <input id="txtFolderName" class="form-control" name="folder" type="text">
                    </div>
                </div>
                <div class="col-12 cleafix">
                    <div class="form-group foot pull-right">
                        <button type="button" class="btn btn-default btn-secondary" onclick="closeAddFolder();">Close</button>
                        <button type="submit" class="btn btn-default ml-1">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/modal/bootbox.min.js") }}" type="text/javascript"></script>

    @if(Plugin::isActive("Groups"))
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>

    <script>
      // Drag Handles
      let drake = dragula([document.getElementById("left-handles")], {
        moves: function (el, container, handle) {
          return handle.classList.contains('handle');
        }
      });
      drake.on('drop', function (el) {
        console.log("droped");
        let list = $("#left-handles .card");


        let array = [];
        $.each(list, function (key, value) {
          let id = $(value).data("item-id");
          let item = {
            id: id,
            position: key + 1
          };

          array.push(item);

        });


        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
          data: {
            array
          },
          dataType: 'json',
          method: 'post',
          url: "{{ route("admin.SaveFoldersPosition") }}",
          cache: false,
          success: function (html) {
            toastr.success('Pozice složek byla úspěšně uložena.', 'Uloženo');
          }
        });
      });
    </script>
    @endif
    <script>
        let addFolderDialog;

        function closeAddFolder() {
            addFolderDialog.modal('hide');
        }

        $(document).ready(function() {
            let addFolderHtml = $('.js-add-folder').html();
            $('.js-add-folder').remove();

            $('.addFolder').on('click', function (e) {
                e.preventDefault();
                let link = $(this).attr('href');

                addFolderDialog = bootbox.dialog({
                    message: addFolderHtml,
                    title: "{{ __('blocks.folderModalTitle') }}",
                    size: 'small'
                }).on("shown.bs.modal", function(e) {
                    $('#frmAddFolder').on('submit', function (e) {
                        e.preventDefault();
                        let form_locale = $(this).find('select[name="form_locale"]').val();
                        let folder = $(this).find('input[name="folder"]').val();
                        folder = folder.trim();

                        if (folder === false || folder === null) {
                            return false;
                        }

                        if (folder === "") {
                            swal("{{ __('blocks.folderModalError') }}", "", "error");
                            return false;
                        }

                        window.location.href = link + `?form_locale=${encodeURI(form_locale)}&folder=${encodeURI(folder)}`;
                    });
                });
            });

            $('.editFolder').on('click', function (e) {
                e.preventDefault();
                let link = $(this).attr('href');

                addFolderDialog = bootbox.dialog({
                    message: addFolderHtml,
                    title: "{{ __('blocks.folderModalTitle') }}",
                    size: 'small'
                }).on("shown.bs.modal", function(e) {
                    $('#frmAddFolder').on('submit', function (e) {
                        e.preventDefault();
                        let form_locale = $(this).find('select[name="form_locale"]').val();
                        let folder = $(this).find('input[name="folder"]').val();
                        folder = folder.trim();

                        if (folder === false || folder === null) {
                            return false;
                        }

                        if (folder === "") {
                            swal("{{ __('blocks.folderModalError') }}", "", "error");
                            return false;
                        }

                        window.location.href = link + `&form_locale=${encodeURI(form_locale)}&folder=${encodeURI(folder)}`;
                    });
                });
            });

            $('.removeFolder').on('click', function (e) {
                e.preventDefault();
                let link = $(this).attr('href');

                swal({
                    title: "{{__('blocks.FolderRemoveModalTitle')}}",
                    text: "{{ __('blocks.FolderRemoveModalDescription') }}",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{ __('blocks.FolderRemoveModalNoButton') }}",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: false,
                        },
                        confirm: {
                            text: "{{ __('blocks.FolderRemoveModalYesButton') }}",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                        swal({
                            title: '{{ __('blocks.FolderSuccessModalTitle') }}',
                            text: " ",
                            icon: "success",
                            buttons:false,
                        });
                        setTimeout(function(){
                            window.location.href = link;
                        }, 1000);
                    } else {
                        swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                    }
                });
            });
        });
    </script>
@endsection
