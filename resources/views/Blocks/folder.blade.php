@extends('Admin.layout')

@section("styles")
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
@endsection
@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">{{ $folder->folder }}</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.blocks') }}">Obsah</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.RenderFolder', ['id' => $overFolder->id]) }}">{{ $overFolder->folder }}</a></li>
            <li class="breadcrumb-item active">{{ $folder->folder }}</li>
          </ol>
        </div>
      </div>
    </div>
    @if(Auth::check() && Auth::user()->isAdmin())
      <div class="content-header-right col-md-6 col-12 mb-2">
        <a href="{{ route('admin.RenderBlockForm', ['folder' => $folder->id]) }}"
           class="btn btn-primary pull-right btn-block-sm">@lang('blocks.block.createBlockButton')</a>
      </div>
    @endif
  </div>
  <div class="content-body">
    <section id="card-drag-handle">
      <div class="wrapper">
        @if(session('created'))
          <div class="alert bg-success alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <strong>@lang('blocks.blockCreatedNotif')</strong> @lang('blocks.blockCreatedNotifRest')
          </div>
        @endif
        @if(session('edited'))
          <div class="alert bg-success alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <strong>@lang('blocks.blockEditNotif')</strong>
          </div>
        @endif
        @if(session('duplicated'))
          <div class="alert bg-success alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <strong>@lang('blocks.duplicatedNotif')</strong>
          </div>
        @endif
        <div class="row" id="left-handles">
          @if(count($blocks))
            @foreach($blocks as $block)
              <div class="col-lg-3 movable">
                <div class="blocks-folder card" data-item-id="{{ $block->id }}">
                  <div class="card-header fixed-header">
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li>
                          <span class="handle ft-move" style="padding: 0 8px"></span>
                        </li>
                        <li>
                          <a href="{{ route('admin.RenderEditBlock', ['id' => $block->id]) }}" data-toggle="tooltip" title="Upravit"><span class="ft-edit"
                                                                                               style="color: orange"></span></a>
                        </li>
                        <li>
                          <a href="{{ route('admin.DuplicateBlock', ['id' => $block->id]) }}" data-toggle="tooltip" title="Duplikovat"><span class="ft-layers"
                                                                                               style="color: blue"></span></a>
                        </li>
                        <li>
                          <a href="{{ route('admin.RemoveBlock', ['block' => $block->id]) }}" class="removeBlock" data-toggle="tooltip" title="Odstranit"><span
                              class="ft-trash-2" style="color: red"></span></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-content">
                    <div class="card-body text-center">
                      <h4 class="card-title mb-1">{{ $block->name }}</h4>
                      @if(Auth::check() && Auth::user()->isAdmin())
                        <code>{{ $block->system_name }}</code>
                      @endif
                      <br>
                      <a href="{{ route('admin.RenderEditBlock', ['id' => $block->id]) }}" class="btn btn-link">Upravit blok</a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          @else
            <div class="col-lg-3 col-md-12">
              <a href="{{ route('admin.RenderBlockForm', ['folder' => $folder->id]) }}" class="card blocks-folder">
                <div class="card-content">
                  <div class="card-body text-center">
                    <h4 class="card-title">@lang('blocks.block.createBlock')</h4>
                    <button class="btn btn-primary mt-2">@lang('blocks.block.createBlockButton')</button>
                  </div>
                </div>
              </a>
            </div>
          @endif
        </div>
      </div>
    </section>
  </div>

@endsection

@section("scripts")
  <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>
  <script>
    $(document).ready(function () {
      $('.addFolder').on('click', function (e) {
        e.preventDefault();
        let link = $(this).attr('href');


        swal("{{ __('blocks.folderModalTitle') }}", {
          content: "input",
          button: {
            text: "{{ __('blocks.folderModalButton') }}"
          },
        })
          .then((value) => {
            if (value === false || value === null) return false;
            if (value === "") {
              swal("{{ __('blocks.folderModalError') }}", "", "error");
              return false;
            }
            window.location.href = link + `?folder=${encodeURI(value)}`;
          });
      });

      $('.removeBlock').on('click', function (e) {
        e.preventDefault();
        let link = $(this).attr('href');

        swal({
          title: "{{__('blocks.removeModalTitle')}}",
          text: "{{ __('blocks.removeModalDescription') }}",
          icon: "warning",
          buttons: {
            cancel: {
              text: "{{ __('blocks.removeModalNoButton') }}",
              value: null,
              visible: true,
              className: "",
              closeModal: false,
            },
            confirm: {
              text: "{{ __('blocks.removeModalYesButton') }}",
              value: true,
              visible: true,
              className: "",
              closeModal: false
            }
          }
        }).then(isConfirm => {
          if (isConfirm) {
            swal({
              title: '{{ __('blocks.successModalTitle') }}',
              text: " ",
              icon: "success",
              buttons: false,
            });
            setTimeout(function () {
              window.location.href = link;
            }, 1000);
          } else {
            swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
          }
        });
      });

      // Drag Handles
      let drake = dragula([document.getElementById("left-handles")], {
        moves: function (el, container, handle) {
          return handle.classList.contains('handle');
        }
      });
      drake.on('drop', function (el) {
        console.log("droped");
        let list = $("#left-handles .card");


        let array = [];
        $.each(list, function (key, value) {
          let id = $(value).data("item-id");
          let item = {
            id: id,
            position: key + 1
          };

          array.push(item);

        });


        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
          data: {
            array
          },
          dataType: 'json',
          method: 'post',
          url: "{{ route("admin.SaveBlocksPosition") }}",
          cache: false,
          success: function (html) {
            toastr.success('Pozice bloků byla úspěšně uložena.', 'Uloženo');
          }
        });
      });
    });
  </script>
@endsection