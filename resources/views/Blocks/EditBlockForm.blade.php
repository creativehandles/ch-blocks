@extends('Admin.layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/summernote/summernote-lite.css") }}">
@endsection
@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">{{$block->name}}</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.blocks') }}">Obsah</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.RenderFolder', ['id' => $overFolder->id]) }}">{{ $overFolder->folder }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.RenderSubFolder', ['subfolder' => $folder->id, 'folder' => $overFolder->id]) }}">{{ $folder->folder }}</a></li>
            <li class="breadcrumb-item active">{{$block->name}}</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body">
    <section id="ajax">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title"
                  id="basic-layout-form">@lang('blocks.block.creation.createBlockTitle')</h4>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">

                <form class="form" id="form" method="post" action="{{ route('admin.SaveBlock') }}">
                  @csrf
                  <input type="hidden" name="folderID" value="{{ $folder->id }}">
                  <input type="hidden" name="blockID" value="{{ $block->id }}">
                  <div class="form-body">
                      @include('Admin.partials.multi-language-select-locale', ['model' => $block])
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="projectinput1">@lang('blocks.block.creation.createBlockHeading')</label>
                          <input
                            type="text"
                            class="form-control always-show-maxlength block-heading"
                            id="maxlength-always-show"
                            value="{{$block->name}}"
                            placeholder="@lang('blocks.block.creation.createBlockHeadingPlaceholder')"
                            maxlength="100"
                            name="title"/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="projectinput1">@lang('blocks.block.creation.createBlockSystemName')</label>
                          <input type="text"
                                 class="form-control block-system-name"
                                 value="{{ $block->system_name }}"
                                 disabled
                                 readonly/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="summernote">@lang('blocks.block.creation.createBlockContent')</label>
                          @if($block->textarea == 0)
                          <textarea id="summernote" name="editordata">{{ $block->content }}</textarea>
                          <input type="hidden" id="html" name="html">
                          @else
                            <textarea id="summernote" class="form-control" name="html">{{ strip_tags($block->content) }}</textarea>
                          @endif
                        </div>
                      </div>
                    </div>
                    @if(Plugin::isActive('Gallery'))
                      {!! Gallery::RenderChooseButton($block->image) !!}
                    @endif
                  </div>
                  <div class="form-actions">
                    <a href="{{ URL::previous() }}" class="btn btn-outline-light mr-1">
                      <i class="ft-x"></i> @lang('blocks.back')
                    </a>
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-check-square-o"></i> @lang('blocks.block.creation.createButton')
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection

@section("scripts")
  <script src="{{ asset("vendors/js/forms/extended/maxlength/bootstrap-maxlength.js") }}"
          type="text/javascript"></script>
  <script src="{{ asset("js/scripts/forms/extended/form-maxlength.js") }}" type="text/javascript"></script>
  <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/summernote/summernote-lite.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/summernote/lang/summernote-cs-CZ.min.js") }}"
          type="text/javascript"></script>
  <script src="{{ asset("js/scripts/functions.js") }}" type="text/javascript"></script>
  @if($block->textarea == 0)
  <script>
    $('#summernote').summernote({
      lang: 'cs-CZ',
      height: 300
    });

    $("#form").on("submit", function (e) {
      let markupStr = $('#summernote').summernote('code');
      $("#html").val(markupStr);
    });
  </script>
  @endif
  <script>
      $(document).ready(function () {
          $('select[name="form_locale"]').on('change', function (e) {
              let slctLocale = $(this);
              let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
              let blockId = "{{ $block->id }}";
              let form_locale = slctLocale.val();

              $.ajax({
                  data: {_token: CSRF_TOKEN, block_id: blockId, "form_locale": form_locale},
                  method: 'GET',
                  url: '{{ route('admin.AjaxEditBlock', ['id' => $block->id]) }}',
              }).done(function (object) {
                  if (object.name) {
                      slctLocale.closest('form').find('input[name="title"]').val(object.name);
                  }

                  if (object.content) {
                      @if($block->textarea == 0)
                          $('#summernote').summernote('code', object.content);
                      @else
                          slctLocale.closest('form').find('input[name="html"], textarea#summernote').val(object.content);
                      @endif
                  }

                  if (object.image) {
                      slctLocale.closest('form').find('input[name="image"]').val(object.image);
                  }

                  if (object.image && object.thumbnail_200) {
                      slctLocale.closest('form').find('#ChoosenThumnail img.img-thumbnail').attr('src', object.thumbnail_200)
                      slctLocale.closest('form').find('#ChoosenThumnail').removeClass('hidden');
                  } else {
                      slctLocale.closest('form').find('#ChoosenThumnail').addClass('hidden');
                  }
              }).fail(function (error) {
                  console.log(error);
                  let messages = error.responseJSON.msg;

                  for (let message in messages) {
                      toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                  }
              });
          });
      });
  </script>
@endsection
