<?php

namespace Creativehandles\ChBlocks\Http\Controllers\PluginsControllers;

use App\Http\Controllers\Controller;
use Creativehandles\ChBlocks\Plugins\Blocks\{Blocks as Blocks,Models\Block,Models\Folder,Models\Overfolder};
use Creativehandles\ChGallery\Plugins\Gallery\Gallery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BlocksController extends Controller
{

    protected $blocks;
    protected $views = 'Admin.Blocks.';

    public function __construct(Request $request)
    {
        // Assign blocks plugin into variable
        $this->blocks = new Blocks();
    }

    /**
     * Render blocks index - folders page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view($this->views . "index")
            ->with("erasedFolders", $this->blocks->GetErasedOverFolders())
            ->with("folders", $this->blocks->GetOverFolders());
    }

    /**
     * Creates folder for blocks
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateFolder(Request $request)
    {
        return $this->blocks->createSystemFolder(new Overfolder(), $request);
    }

    /**
     * Creates subfodlerfolder for blocks
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateSubFolder(Request $request, $id)
    {
        return $this->blocks->createSystemFolder(new Folder(), $request, ['overfolder' => $id]);
    }

    /**
     * Render grid of block inside folder
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderFolder($id)
    {
        return view($this->views . 'folders')
            ->with('overFolder', $this->blocks->getOverfolder($id))
            ->with("erasedFolders", $this->blocks->GetErasedFolders())
            ->with('folders', $this->blocks->getOrderedFoldersInFolder($id, 'ASC'));
    }

    /**
     * Render grid of block inside folder
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderSubFolder($folder, $subfolder)
    {
        return view($this->views . 'folder')
            ->with('overFolder', $this->blocks->getOverfolder($folder))
            ->with('folder', $this->blocks->getFolder($subfolder))
            ->with('blocks', $this->blocks->getOrderedBlocksInFolder($subfolder, 'ASC'));
    }


    /**
     * Render template for block creation
     *
     * @param $folder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderBlockForm($folder)
    {
        return view($this->views . 'blockForm')
            ->with('folder', $this->blocks->getFolder($folder));
    }

    /**
     * Save or Edit block in the DB
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function SaveBlock(Request $request)
    {
        $status = $this->blocks->save($request->all());
        if ($status) {
            $notif = isset($request->blockID) ? 'edited' : 'created';
            return redirect(route('admin.RenderSubFolder', [
                'subfolder' => $request->folderID,
                'folder' => $this->blocks->getSubfoldersOverFolder($request->folderID)->id
            ]))->with($notif, true);
        }
    }

    /**
     * Softly removes the block by given ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function RemoveBlock(Block $block)
    {
        $block->delete();
        return back();
    }

    /**
     * Render template with block edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function RenderEditBlock($id)
    {
        $block = $this->blocks->getBlock($id);
        $folder = $this->blocks->getFolder($block->folder);
        $overFolder = $this->blocks->getOverfolder($folder->overfolder);

        return view($this->views . 'EditBlockForm')
            ->with('folder', $folder)
            ->with('block', $block)
            ->with('overFolder', $overFolder);
    }

    public function SaveBlocksPosition(Request $request)
    {
        if ($this->blocks->savePosition($request->array)) {
            return response()->json(['status' => true]);
        }

        return response()->json(['status' => false]);
    }

    public function renameOverfolder(Request $request)
    {
        return back()->with(
            "success",
            $this->blocks->renameOverfolder($request->id, $request->form_locale, $request->folder)
        );
    }

    public function RenameFolder(Request $request)
    {
        return back()->with(
            "success",
            $this->blocks->renameFolder($request->id, $request->form_locale, $request->folder)
        );
    }


    /**
     * Softly removes folder and its content
     *
     * @param $id
     * @return $status
     */
    public function RemoveFolder($id)
    {
        $status = $this->blocks->removeFolder($id);
        return back()->with('status', $status);
    }

    /**
     * Softly removes folder and its content
     *
     * @param $id
     * @return $status
     */
    public function RemoveSubFolder($id)
    {
        $status = $this->blocks->RemoveSubFolder($id);
        return back()->with('status', $status);
    }


    public function RestoreFolder($id)
    {
        $status = $this->blocks->restoreFolder($id);
        return back()->with("status", $status);
    }

    public function RestoreOverFolder($id)
    {
        $status = $this->blocks->restoreOverFolder($id);
        return back()->with("status", $status);
    }

    /**
     * Makes a copy of block
     *
     * @param $id
     *
     * @return View;
     */
    public function DuplicateBlock($id)
    {
        $this->blocks->duplicateBlock($id);

        return back()->with("duplicated", true);
    }

    /**
     * Returns a Block instance as a JSON string
     *
     * @param int $id Block ID
     *
     * @return JsonResponse
     */
    public function EditBlock(Request $request, int $id)
    {
        $id = $request->get('block_id');
        $locale = $request->get('form_locale');
        $block = $this->blocks->getBlock($id, $locale);

        if ($block === null) {
            return response()->json(['msg' => [__('blocks.block.notFound')]], 404);
        }

        return response()->json(['locale' => $block->locale, 'name' => $block->name, 'content' => $block->content, 'image' => $block->image, 'thumbnail_200' => Gallery::thumbnail($block->image, 200)]);
    }
}
