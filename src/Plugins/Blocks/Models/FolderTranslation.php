<?php

namespace Creativehandles\ChBlocks\Plugins\Blocks\Models;

use Illuminate\Database\Eloquent\Model;

class FolderTranslation extends Model
{
    protected $table = 'blocks_folder_translations';
    public $fillable = ['folder'];
}
