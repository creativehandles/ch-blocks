<?php

namespace Creativehandles\ChBlocks\Plugins\Blocks\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Translatable\HasTranslations;

class Folder extends Model
{
    use HasTranslations, SoftDeletes;

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub


        // extend deleting function
        static::deleting(function($model)
        {
            // keep who deletes the record
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

    protected $dates = ['deleted_at'];
    protected $table = 'blocks_folder';
    public $fillable = ['system_name', 'overfolder', 'group', 'position','folder'];

    public $translationModel = "Creativehandles\ChBlocks\Plugins\Blocks\Models\FolderTranslation";
    public $translationForeignKey = "blocks_folder_id";
     public $translatable = ['folder'];

    public function blocks()
    {
        return $this->hasMany(Block::class,'folder','id');
    }


    public function parentFolder()
    {
        return $this->belongsTo(Overfolder::class,'overfolder','id');
    }
}
