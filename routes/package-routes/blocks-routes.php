<?php

/**
 * Groups plugin routes
 */
Route::group(['name' => 'group', 'groupName' => 'group'], function () {
    Route::get('/createGroup', 'PluginsControllers\GroupController@CreateGroup')->name('CreateGroup');
    Route::get('/assignGroup', 'PluginsControllers\GroupController@AssignGroup')->name('AssignGroup');
    Route::post('/SaveFoldersPosition', 'PluginsControllers\GroupController@SaveFoldersPosition')->name('SaveFoldersPosition');
});

/**
 * Blocks plugin routes
 */
Route::group(['name' => 'blocks', 'groupName' => 'blocks'], function () {
    Route::get('/blocks', 'PluginsControllers\BlocksController@index')->name('blocks');
    Route::get('/createBlockFolder', 'PluginsControllers\BlocksController@CreateFolder')->name('CreateFolder');
    Route::get('/createBlockFolder/{id}', 'PluginsControllers\BlocksController@CreateSubFolder')->name('CreateSubFolder');
    Route::get('/folder/{id}', 'PluginsControllers\BlocksController@RenderFolder')->name('RenderFolder');
    Route::get('/folder/{folder}/subfolder/{subfolder}', 'PluginsControllers\BlocksController@RenderSubFolder')->name('RenderSubFolder');
    Route::get('/renameOverfolder', 'PluginsControllers\BlocksController@renameOverfolder')->name('renameOverfolder');
    Route::get('/renameFolder', 'PluginsControllers\BlocksController@RenameFolder')->name('RenameFolder');
    Route::get('/createBlock/{folder}', 'PluginsControllers\BlocksController@RenderBlockForm')->name('RenderBlockForm');
    Route::post('/saveBlock', 'PluginsControllers\BlocksController@SaveBlock')->name('SaveBlock');
    Route::get('/removeBlock/{block}', 'PluginsControllers\BlocksController@RemoveBlock')->name('RemoveBlock');
    Route::get('/removeFolder/{id}', 'PluginsControllers\BlocksController@RemoveFolder')->name('RemoveFolder');
    Route::get('/removeSubFolder/{id}', 'PluginsControllers\BlocksController@RemoveSubFolder')->name('RemoveSubFolder');
    Route::get('/editBlock/{id}', 'PluginsControllers\BlocksController@RenderEditBlock')->name('RenderEditBlock');
    Route::get('/block/{id}', 'PluginsControllers\BlocksController@EditBlock')->name('AjaxEditBlock');
    Route::post('/saveBlocksPosition', 'PluginsControllers\BlocksController@SaveBlocksPosition')->name('SaveBlocksPosition');
    Route::get('/restoreFolder/{id}', 'PluginsControllers\BlocksController@RestoreFolder')->name('RestoreFolder');
    Route::get('/restoreOverFolder/{id}', 'PluginsControllers\BlocksController@RestoreOverFolder')->name('RestoreOverFolder');
    Route::get('/duplicateBlock/{id}', 'PluginsControllers\BlocksController@DuplicateBlock')->name('DuplicateBlock');
});
